# General Introduction

This repository is meant to perform the calibration fit on the output of GemMonitor (the TCSphase-plots) which you can for example clone [here](http://fsketzer.cb.uni-bonn.de/gitlab/RoReRa/gemMonitor_local). In GemMonitor you need an output created by analyzing data of a latency scan. The latency scan is regularly done at the [COMPASS experiment](http://wwwcompass.cern.ch/) and is mentioned in the [Hitchhiker's Guide](https://gitlab.cern.ch/compass-gem/hitchhikers-guide).

# Latency Scan
The latency scan is a method to find out the timing between the creation of the trigger signal and the readout of the APV values.
A latency is digitally set in the frontend database and can be adjusted, if necessary. 

# What to do in GemMonitor
In GemMonitor you can create the output by listing the .raw data files in a GemMonitor file list which has to list all of the .raw files produced in the latency scan. A GemMonitor file list start with a line '### gemMonitor file list', which will be detected and recognized as data source by GemMonitor. After that you need to give all of the data files and their corresponding latency as "...","-1","+0","+1", and so on, seperated by a blank.
Starting GemMonitor with the command "bin/gemMonitor config/YEAR/gemMonitor.config.(p)gem &", where the directory is a configuration file, you can give the directory to the file list as a path in the field "Data Source". Choosing every Gem-plane in the menu "Modules" you can start a run. Keep in mind, that in the calibration file a directory for the pedestal files should be specified, else you will create wrong data. After the run go to the menu "Histograms" and "Save as..." to produce the needed output this script can work on.

# How the Repository works
For every year (starting 2016) I intend to create a new branch from master (or master being the current year). The scripts to create the calibrations and results for each year are tracked by git. The history-files "something.root" are not tracked by this repository and should be created for each year anew if necessary.

# How to use the Script
Before you execute the Script you need to make some modifications in the code:

* At the very beginning in line 8 you need to specify the histogram-file you want to investigate.
* In line 28 and 29 you probably want to specify which detector planes you want to investigate (default should be all planes). 
* Also in line 31, 69, 155 and 169 you should specify if you want to loop over GEMs or PGEMs.
* Lastly in line 156 and 170 the range of validity of the calibration files to be created needs to be adjusted in terms of the convention like "__timing\~\~start-2018-01-01-00:00:00\~\~finish-2018-12-31-23:59:59".

Now you can open a root CINT and execute the script with ".x tcfit.C".


# How the Script works
In the beginning this script takes a root directory file created by GemMonitor. The script loops through all directories (which are hardcoded for all gems), finds starting parameters, performs a 2D-fit and prints out the fit results as a .pdf file with control plots as well as the time calibration files needed for the compass data base.
Moreover the script is structured as following:
* Specifying of the input histogram root-directory-file from GemMonitor
* Hard-coded Arrays of these directories
* Initiation of loops over GEM-planes and plane-coordinates
* Definition of needed histograms, variables and implementation of the fit-function
* Sections of implementations to estimate starting parameters for the fit which are thought of as stable and "always-working" 1-dimensional gaussian fits to the data in the TCS-phase plots. They are sorted as following:
   1. fit to a late Y-projection in the TCS-phase plot
   2. fit to an early Y-projection in the TCS-phase plot
   3. fit along a X-projection of half the value of the mean from step 1
   4. iterating over fits to Y-projections, beginning at step 1, till the mean of the fit varies by more than 0.05
   5. set parameters and perform fit
   6. get and print the fit results
   7. do time reconstruction from data in TCS-phase plots as a crosscheck
* Closing of initiated loops
This implementation works stable for the 2018 latency scan and provides a reasonable time calibration __(commented: September 2018)__. For the years 2016 and 2017 the script does not work automatically cause of the quality of data as it seems __(commented: 10. October 2018)__ .





